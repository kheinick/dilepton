#! /usr/bin/env python2

import ROOT
import matplotlib.pyplot as plt
import numpy as np


def callWithDict(obj, d):
    for arg in d:
        val = d[arg]
        if type(val) == dict:
            getattr(obj, arg)(**val)
        else:
            getattr(obj, arg)(val)


def plotDiffs(a, b, branches=None, config=None):
    """Plotting (a - b) and a in histograms

    Arguments:
    a, b: the arrays to be plotted. Each, (a[branch] - b[branch]) and a
          histogram will be produced and stored at a certain location.

    Keyword arguments:
    branches: A list of branches to use. By default every branch will be
              plotted.
    config: A configuration object. It should consist of objects naming the
            branch. Each branch-object can hold sub-objects for configuring
            the plots.
            Example: config = {
                'm': {
                    'diff': {
                        'plt_kwargs': {
                            'legend': {'location': best}
                        }
                    },
                    'hist': {
                        'hist_kwargs': {
                            'bins': 42
            } } } }
    """
    f, ax = plt.subplots()

    if not branches:
        branches = np.intersect1d(a.dtype.names, b.dtype.names)

    # Iterate branches to plot everything
    for branch in branches:
        diff = a[branch] - b[branch]

        # configuration for hist object
        hist_args = {
            'bins': 50,
            'histtype': 'step'
        }
        try:
            hist_args.update(config[branch]['diff']['hist_kwargs'])
            if config[branch]['diff']['normed']:
                diff = diff / b[branch]
        except:
            pass

        plt.hist(diff, **hist_args)
        plt.title(r'$\Delta$' + branch)

        # configuration for plt object
        try:
            callWithDict(plt, config[branch]['diff']['plt_kwargs'])
        except:
            pass

        plt.tight_layout()
        plt.savefig('build/plots/notebooks/diff-{}.pdf'.format(branch))
        plt.clf()

        # -- default variable histogram --
        # setup histogram arguments
        hist_args = {
            'bins': 50,
            'histtype': 'step'
        }

        try:
            hist_args.update(config[branch]['hist']['hist_kwargs'])
        except:
            pass

        plt.hist(a[branch], **hist_args)
        plt.title(branch)

        # setup plt-object arguments
        try:
            callWithDict(plt, config[branch]['hist']['plt_kwargs'])
        except:
            pass

        plt.tight_layout()
        plt.savefig('build/plots/notebooks/hist-{}.pdf'.format(branch))
        plt.clf()
