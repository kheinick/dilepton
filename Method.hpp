#ifndef METHOD_HPP_AOLAHOQ6
#define METHOD_HPP_AOLAHOQ6

#include <iostream>
#include <map>

#include "TTree.h"
#include "TNtuple.h"

#include "Topreco/Methods/IReconstructionMethod.h"

struct Method {
  /* Default constructor
   */
  Method(){};

  /* Copy constructor
   * neccessar for multithreading
   */
  Method(Method* copiedMethod) :
    method(copiedMethod->method->copyPionter()),
    name(copiedMethod->name),
    baseDirectory(copiedMethod->baseDirectory),
    counters(copiedMethod->counters),
    tree(copiedMethod->tree),
    calculations(copiedMethod->calculations),
    customCalculations(copiedMethod->customCalculations),
    _ntuple(copiedMethod->_ntuple)
  {};

  IReconstructionMethod* method;
  std::string name;
  std::string baseDirectory;
  std::map<std::string, int> counters;
  TTree *tree = nullptr;
  Jet top,
      topBar;
  std::map<std::string, double (Jet::*)() const> calculations;
  std::map<std::string, double> customCalculations;
  void writeCalculations(){
    if(_ntuple){
      // store all calculations in a vector
        std::vector<float> calcs;
        auto p = top;
        calcs.push_back(p.user_index());
        for(auto c : calculations){
          calcs.push_back((p.*c.second)());
        }
        for(auto c : customCalculations){
          calcs.push_back(c.second);
        }
        // vec2array, fill the tuple
        float *values = &calcs[0];
        _ntuple->Fill(values);
    } else {
      if(_createNtuple()){
        writeCalculations();
      }
    }
  };
  void initializeTTree(){
    _createNtuple();
  }

  private:
    TNtuple *_ntuple = nullptr;

    /* Just create a Ntuple, if it does not exist yet
     */
    bool _createNtuple(){
      if(tree && !tree->GetBranch(name.c_str())){
        std::stringstream ss;
        ss << "id";
        for(auto c : calculations){
          ss << ":" << c.first;
        }
        for(auto c : customCalculations){
          ss << ":" << c.first;
        }
        _ntuple = new TNtuple(name.c_str(), name.c_str(), ss.str().c_str());
        return true;
      } else if (tree){
        return true;
      } else {
        BOOST_LOG_TRIVIAL(error)
          << "You want to access a TTree, but did not set it";
        return false;
      }
    }
};

#endif
