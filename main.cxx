#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <map>
#include <thread>
#include <memory>

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TNtuple.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TTreeReaderArray.h"
#include "TH1D.h"
#include "TCanvas.h"

#include "boost/program_options.hpp"

#include "Topreco/PrepareParticle.h"
#include "Topreco/Topreco.h"
#include "Topreco/Methods/Truth.h"
#include "Topreco/Methods/NeutrinoWeighting.h"
#include "Topreco/MyPseudoJet.h"
#include "Method.hpp"

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/program_options.hpp>

typedef MyPseudoJet Jet;
typedef std::tuple<double, double, double, double, int, int> particle_structure_t;


static int limit;
static size_t threadNumber,
              etaSampleSize,
              topMassSampleSize;
static double bestWeightCutoff;
static std::vector<std::string> cmdLineMethods = {"truth", "pseudo", "nw"};
static std::vector<std::string> nwTruthArgumentList = {};
std::string storageFileName;
static bool newNwMethod,
            storeEtaDistributions,
            scanEta;

int init(int &argc, char** &argv) {
  int verbosity;

  namespace po = boost::program_options;

  po::positional_options_description p;
  p.add("reco-method", -1);

  po::options_description desc("Available commands");
  desc.add_options()
    ("help,h", "Produce this help message")
    ("verbosity,v",
     po::value<int>(&verbosity)->default_value(0),
     "Set level of verbosity")
    ("limit,l",
     po::value<int>(&limit)->default_value(0),
     "Limit the tuple-iteration")
    ("jobs,j",
     po::value<size_t>(&threadNumber)->default_value(1),
     "Set the number of Threads created")
    ("reco-method",
     po::value<std::vector<std::string>>(&cmdLineMethods),
     "Set the methods to use. Available [truth, pseudo, nw]")
    ("storage-file,S",
     po::value<std::string>(&storageFileName)->default_value("build/storage.root"),
     "Set storag-file.")
    ("nw-truth-values,T",
     po::value<std::vector<std::string>>(&nwTruthArgumentList)->multitoken(),
     "Turn on the usage of different true values. Chose any out of [TopMass, "
     "Bmass, Bcharge, Wmass, Eta]")
    ("nw-eta-sample",
     po::value<size_t>(&etaSampleSize)->default_value(30),
     "Set the number of sampled etas")
    ("nw-topmass-sample",
     po::value<size_t>(&topMassSampleSize)->default_value(30),
     "Set the number of sampled top-masses")
    ("nw-new",
     po::value<bool>(&newNwMethod)->default_value(true),
     "Do you want to use new method?")
    ("nw-best-weight-cutoff",
     po::value<double>(&bestWeightCutoff)->default_value(0.999),
     "Set cutoff for NW algorithm weight.")
    ("nw-store-eta-dist",
     po::value<bool>(&storeEtaDistributions)->default_value(false),
     "Chose whether histograms of eta distributions should be stored.")
    ("nw-scan-eta",
     po::value<bool>(&scanEta)->default_value(false),
     "Chose whether to scan eta instead of sampling with gaussian distribution.")
  ;
  po::variables_map varmap;
  po::store(po::command_line_parser(argc, argv)
            .options(desc)
            .positional(p).run(), varmap);
  po::notify(varmap);

  if(varmap.count("help")){
    BOOST_LOG_TRIVIAL(debug) << desc;
    return 0;
  }

  auto s = boost::log::trivial::fatal;
  if(varmap.count("verbosity")){
    verbosity = varmap["verbosity"].as<int>();
  }
  switch(verbosity){
    case 1:
      s = boost::log::trivial::error;
      break;
    case 2:
      s = boost::log::trivial::warning;
      break;
    case 3:
      s = boost::log::trivial::info;
      break;
    case 4:
      s = boost::log::trivial::debug;
      break;
    case 5:
      s = boost::log::trivial::trace;
      break;
    default:
      break;
  }
  boost::log::core::get()->set_filter(boost::log::trivial::severity >= s);
  return 1;
}

void runMethods(
    std::vector<particle_structure_t> particleVector,
    PrepareParticle prepper,
    Topreco reco,
    std::vector<Method*> methods
){
  // vectors holding final state particles/jets
  std::vector<Jet> chargedLeptons, jets, neutrinos;

  // prepare all particles in the event and pass them to the
  // reconstruction alg
  for(auto particle : particleVector){
    prepper.addParticle(
      std::get<0>(particle),
      std::get<1>(particle),
      std::get<2>(particle),
      std::get<3>(particle),
      std::get<4>(particle),
      std::get<5>(particle)
    );
  }
  reco.setJets(prepper.getJets(),
               prepper.getChargedLeptons(),
               prepper.getNeutrinos());

  // loop over methods and fill topPairs-vector with the resulting
  // top pairs
  std::vector<std::vector<Jet>> topPairs;
  for(auto method : methods){
    BOOST_LOG_TRIVIAL(info) << method->method->description();
    reco.setMethod(method->method);
    auto topPair = reco.getReconstructedTops();
    auto t = reco.getReconstructedTop();
    auto tBar = reco.getReconstructedTopBar();
    BOOST_LOG_TRIVIAL(info) << t.print(true);
    BOOST_LOG_TRIVIAL(info) << tBar.print(true);
    topPairs.push_back(topPair);

    method->customCalculations["tBar_m"] = tBar.m();
    method->customCalculations["tBar_pt"] = tBar.pt();
    method->customCalculations["tBar_px"] = tBar.px();
    method->customCalculations["tBar_py"] = tBar.py();
    method->customCalculations["tBar_pz"] = tBar.pz();
    method->customCalculations["tBar_eta"] = tBar.eta();
    method->customCalculations["tBar_phi"] = tBar.phi();

    method->top = t;
    method->topBar = tBar;

    Jet ttBarSystem = t + tBar;

    method->customCalculations["ttBar_m"] = ttBarSystem.m();
    method->customCalculations["ttBar_pt"] = ttBarSystem.pt();
    method->customCalculations["ttBar_eta"] = ttBarSystem.eta();

    // angle between b
    Jet b = t.getChild(5, true);
    Jet bBar = tBar.getChild(5, true);
    method->customCalculations["b_delta_R"] = b.delta_R(bBar);
    method->customCalculations["b_delta_phi"] = b.delta_phi_to(bBar);

    // angle between leptons
    Jet l = tBar.getChild(24, true).getChild({11, 13, 15}, true);
    Jet antiL = t.getChild(24, true).getChild({11, 13, 15}, true);
    method->customCalculations["l_delta_R"] = l.delta_R(antiL);
    method->customCalculations["l_delta_phi"] = l.delta_phi_to(antiL);
    method->customCalculations["lm_reco_id"] = l.user_index();
    method->customCalculations["lp_reco_id"] = antiL.user_index();

    // just testing impact of boost
    l.boost(ttBarSystem);
    antiL.boost(ttBarSystem);
    method->customCalculations["l_delta_R_boosted"] = l.delta_R(antiL);
    method->customCalculations["l_delta_phi_boosted"] = l.delta_phi_to(antiL);

    method->customCalculations["lm_b1_delta_R"] = l.delta_R(b);
    method->customCalculations["lm_b2_delta_R"] = l.delta_R(bBar);
    method->customCalculations["lp_b1_delta_R"] = antiL.delta_R(b);
    method->customCalculations["lp_b2_delta_R"] = antiL.delta_R(bBar);
    method->customCalculations["lm_b1_delta_phi"] = l.delta_phi_to(b);
    method->customCalculations["lm_b2_delta_phi"] = l.delta_phi_to(bBar);
    method->customCalculations["lp_b1_delta_phi"] = antiL.delta_phi_to(b);
    method->customCalculations["lp_b2_delta_phi"] = antiL.delta_phi_to(bBar);
    method->customCalculations["lm_pt"] = l.pt();
    method->customCalculations["lp_pt"] = antiL.pt();

    method->customCalculations["ttBar_b_delta_R"] = ttBarSystem.delta_R(b);
    method->customCalculations["ttBar_bBar_delta_R"] = ttBarSystem.delta_R(bBar);
    method->customCalculations["ttBar_lp_delta_R"] = ttBarSystem.delta_R(antiL);
    method->customCalculations["ttBar_lm_delta_R"] = ttBarSystem.delta_R(l);
    method->customCalculations["ttBar_b_delta_phi"] = ttBarSystem.delta_phi_to(b);
    method->customCalculations["ttBar_bBar_delta_phi"] = ttBarSystem.delta_phi_to(bBar);
    method->customCalculations["ttBar_lp_delta_phi"] = ttBarSystem.delta_phi_to(antiL);
    method->customCalculations["ttBar_lm_delta_phi"] = ttBarSystem.delta_phi_to(l);

    // angle between tops
    method->customCalculations["t_delta_phi"] = t.delta_phi_to(tBar);
    method->customCalculations["t_delta_R"] = t.delta_R(tBar);

    Jet Wplus = t.getChild(24, true),
        Wminus = tBar.getChild(24, true);
    method->customCalculations["mW_p"] = t.getChild(24, true).m();
    method->customCalculations["mW_m"] = tBar.getChild(24, true).m();

    Jet nu = t.getChild(24, true).getChild({12, 14, 16}, true),
        nuBar = tBar.getChild(24, true).getChild({12, 14, 16}, true);
    method->customCalculations["nu_pt"] = nu.pt();
    method->customCalculations["nu_px"] = nu.px();
    method->customCalculations["nu_py"] = nu.py();
    method->customCalculations["nu_pz"] = nu.pz();
    method->customCalculations["nu_eta"] = nu.eta();
    method->customCalculations["nu_phi"] = nu.phi();
    method->customCalculations["nuBar_px"] = nuBar.px();
    method->customCalculations["nuBar_py"] = nuBar.py();
    method->customCalculations["nuBar_pz"] = nuBar.pz();
    method->customCalculations["nuBar_pt"] = nuBar.pt();
    method->customCalculations["nuBar_eta"] = nuBar.eta();
    method->customCalculations["nuBar_phi"] = nuBar.phi();
    method->customCalculations["nu_reco_id"] = nu.user_index();
    method->customCalculations["nuBar_reco_id"] = nuBar.user_index();
    method->customCalculations["Et_miss"] = (nu + nuBar).Et();
    method->customCalculations["px_miss"] = (nu + nuBar).px();
    method->customCalculations["py_miss"] = (nu + nuBar).py();
    method->customCalculations["pz_miss"] = (nu + nuBar).pz();
    if(method->baseDirectory == "nw"){
      method->customCalculations["t_weight"] = t.weight;
      method->customCalculations["tb_weight"] = tBar.weight;
    }
  }

  if(methods.size() == 2){
    if(std::abs(methods[0]->customCalculations["nu_eta"]
                - methods[1]->customCalculations["nu_eta"]) > 2){
      BOOST_LOG_TRIVIAL(warning)
        << methods[0]->top.print(true) << methods[0]->topBar.print(true)
        << methods[1]->top.print(true) << methods[1]->topBar.print(true);
    }
  }

  prepper.clear();
}

int main(int argc, char** argv){
  if(!init(argc, argv)){
    return 0;
  }

  // setting up particle preparation
  PrepareParticle prepper;

  // setting up the reader
  TFile *file = new TFile("data/topdecayedLO.root");
  TTreeReader reader("t3", file);
  TTreeReaderValue<int> n(reader, "nparticle");
  TTreeReaderArray<float> px(reader, "px");
  TTreeReaderArray<float> py(reader, "py");
  TTreeReaderArray<float> pz(reader, "pz");
  TTreeReaderArray<float> E(reader, "E");
  TTreeReaderValue<double> weight(reader, "weight");
  TTreeReaderArray<int> kf(reader, "kf");
  TTreeReaderValue<int> id(reader, "id");

  // storage
  TFile *storage = new TFile(storageFileName.c_str(), "recreate");
  // creating canvas and histogram

  // TTree for storing calculated data
  TTree *storageTree = new TTree("storage", "storage");

  // same calculations for all methods
  std::map<std::string, double (Jet::*)() const> calculations;
  calculations["t_m"] = &Jet::m;
  calculations["t_pt"] = &Jet::pt;
  calculations["t_px"] = &Jet::px;
  calculations["t_py"] = &Jet::py;
  calculations["t_pz"] = &Jet::pz;
  calculations["t_eta"] = &Jet::eta;
  calculations["t_phi"] = &Jet::phi;

  std::map<std::string, double> customCalculations;
  customCalculations["tBar_m"] = 0;
  customCalculations["tBar_pt"] = 0;
  customCalculations["tBar_px"] = 0;
  customCalculations["tBar_py"] = 0;
  customCalculations["tBar_pz"] = 0;
  customCalculations["tBar_eta"] = 0;
  customCalculations["tBar_phi"] = 0;
  customCalculations["nu_pt"] = 0;
  customCalculations["nu_px"] = 0;
  customCalculations["nu_py"] = 0;
  customCalculations["nu_pz"] = 0;
  customCalculations["nu_eta"] = 0;
  customCalculations["nuBar_pt"] = 0;
  customCalculations["nuBar_px"] = 0;
  customCalculations["nuBar_py"] = 0;
  customCalculations["nuBar_pz"] = 0;
  customCalculations["nuBar_eta"] = 0;
  customCalculations["l_delta_R"] = 0;
  customCalculations["l_delta_phi"] = 0;
  customCalculations["l_delta_R_boosted"] = 0;
  customCalculations["l_delta_phi_boosted"] = 0;
  customCalculations["b_delta_R"] = 0;
  customCalculations["b_delta_phi"] = 0;
  customCalculations["t_delta_R"] = 0;
  customCalculations["t_delta_phi"] = 0;
  customCalculations["mW_p"] = 0;
  customCalculations["mW_m"] = 0;
  customCalculations["lm_b1_delta_R"] = 0;
  customCalculations["lm_b2_delta_R"] = 0;
  customCalculations["lp_b1_delta_R"] = 0;
  customCalculations["lp_b2_delta_R"] = 0;
  customCalculations["lm_b1_delta_phi"] = 0;
  customCalculations["lm_b2_delta_phi"] = 0;
  customCalculations["lp_b1_delta_phi"] = 0;
  customCalculations["lp_b2_delta_phi"] = 0;
  customCalculations["ttBar_m"] = 0;
  customCalculations["ttBar_pt"] = 0;
  customCalculations["ttBar_eta"] = 0;
  customCalculations["ttBar_b_delta_phi"] = 0;
  customCalculations["ttBar_bBar_delta_phi"] = 0;
  customCalculations["ttBar_lp_delta_phi"] = 0;
  customCalculations["ttBar_lm_delta_phi"] = 0;
  customCalculations["ttBar_b_delta_R"] = 0;
  customCalculations["ttBar_bBar_delta_R"] = 0;
  customCalculations["ttBar_lp_delta_R"] = 0;
  customCalculations["ttBar_lm_delta_R"] = 0;
  customCalculations["lm_reco_id"] = 0;
  customCalculations["lp_reco_id"] = 0;
  customCalculations["nu_reco_id"] = 0;
  customCalculations["nuBar_reco_id"] = 0;
  customCalculations["Et_miss"] = 0;
  customCalculations["px_miss"] = 0;
  customCalculations["py_miss"] = 0;
  customCalculations["pz_miss"] = 0;
  customCalculations["nuBar_phi"] = 0;
  customCalculations["nu_phi"] = 0;
  customCalculations["lm_pt"] = 0;
  customCalculations["lp_pt"] = 0;
  customCalculations["weight"] = 0;

  // define and setup the methods used for reconstruction
  Method trueMethod;
  trueMethod.method = new Truth;
  trueMethod.baseDirectory = "truth";
  trueMethod.name = "Truth";
  trueMethod.tree = storageTree;
  trueMethod.calculations = calculations;
  trueMethod.customCalculations = customCalculations;

  Method pseudoMethod;
  pseudoMethod.method = new PseudoTop;
  pseudoMethod.baseDirectory = "pseudo";
  pseudoMethod.name = "Pseudo";
  pseudoMethod.tree = storageTree;
  pseudoMethod.calculations = calculations;
  pseudoMethod.customCalculations = customCalculations;

  Method neutrinoWeightingMethod;
  neutrinoWeightingMethod.method = new NeutrinoWeighting;
  neutrinoWeightingMethod.baseDirectory = "nw";
  neutrinoWeightingMethod.name = "Neutrino Weighting";
  neutrinoWeightingMethod.calculations = calculations;
  neutrinoWeightingMethod.customCalculations = customCalculations;
  neutrinoWeightingMethod.customCalculations["t_weight"] = 0;
  neutrinoWeightingMethod.customCalculations["tb_weight"] = 0;
  neutrinoWeightingMethod.tree = storageTree;
  neutrinoWeightingMethod.counters["off300"] = 0;
  // custom setup for nw-method
  NeutrinoWeighting *nwmethod = (NeutrinoWeighting *) neutrinoWeightingMethod.method;
  nwmethod->setEtaSampleSize(etaSampleSize);
  nwmethod->setTopSampleSize(topMassSampleSize);
  nwmethod->newNwMethod = newNwMethod;
  nwmethod->setBestWeightCutoff(bestWeightCutoff);
  nwmethod->storeEtaDistributions = storeEtaDistributions;
  nwmethod->scanEta = scanEta;
  for(auto methodKey : nwTruthArgumentList){
    if(nwmethod->useTruth.find(methodKey) != nwmethod->useTruth.end()){
      nwmethod->useTruth[methodKey] = true;
    }
  }

  Topreco reco;

  // setup methods
  std::vector<Method*> methods;
  for(auto method : std::vector<Method*> ({&trueMethod,
                                           &pseudoMethod,
                                           &neutrinoWeightingMethod})
  ){
    if(std::find(cmdLineMethods.begin(),
                 cmdLineMethods.end(),
                 method->baseDirectory) != cmdLineMethods.end()){
      method->initializeTTree();
      methods.push_back(method);
    }
  }

  // create a copy of all methods for every thread
  std::vector<std::vector<Method*>> methodCopies(threadNumber);
  for(auto &copies : methodCopies){
    for(auto &method : methods){
      copies.push_back(new Method(method));
    }
    BOOST_LOG_TRIVIAL(debug) << "Current copies vector contains " << copies.size() << " Methods";
  }
  BOOST_LOG_TRIVIAL(debug) << "there are " << methodCopies.size() << " copied methodvectors";

  int loops = 0,
      failRecos = 0;

  // Main-Loop
  //
  // cache particle data
  std::vector<std::vector<particle_structure_t>>eventCache;
  while(reader.Next() && (!limit || loops < limit)){
    loops++;

    if(eventCache.size() < threadNumber
       && eventCache.size() <
       (size_t)(reader.GetEntries(false) - reader.GetCurrentEntry())){
      std::vector<particle_structure_t> particles;
      for(int i=0; i<*n; i++){
        particle_structure_t particle{px[i], py[i], pz[i], E[i], kf[i], loops*10+i+1};
        particles.push_back(particle);
      }
      eventCache.push_back(particles);
      BOOST_LOG_TRIVIAL(debug) << __FUNCTION__ << " eventCache.size == "
        << eventCache.size();
    }

    // run methods as soon as cache is filled
    if(eventCache.size() == threadNumber
       || (eventCache.size() + reader.GetCurrentEntry())
       == (size_t)(reader.GetEntries(false))){
      std::vector<std::thread> threads;

      // run thread for every particle when cache is filled
      //for(auto& particleVector : eventCache){
      for(size_t i=0; i<eventCache.size(); i++){
        threads.push_back(std::thread(
          runMethods,
          eventCache[i],
          prepper,
          reco,
          methodCopies[i]
        ));
      }
      for(auto &t : threads){
        t.join();
      }
      // Write to file in single threads.
      // This should be fast enough
      for(auto &methodVector : methodCopies){
        for(auto &method : methodVector){
          method->customCalculations["weight"] = *weight;
          method->writeCalculations();
        }
      }
      eventCache.clear();
    }
    BOOST_LOG_TRIVIAL(warning) << reader.GetCurrentEntry();
    storage->Write(storage->GetName(), TObject::kOverwrite);
  }

  for(auto m : methods){
    for(auto kv : m->counters){
      std::cout << kv.first << " " << kv.second << std::endl;
    }
  }
  std::cout << failRecos << " Events could not be reconstructed ("
    << 100 * float(failRecos) / float(loops) << "%)" << std::endl;
  std::cout << "Processed " << loops << " Events."
    << " Writing to file ./" << storage->GetName() << "." << std::endl;
  storage->Close();
  return 0;
}
