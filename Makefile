CC := g++
STD := -std=c++14

FLAGS := -O3 -g -m64

ROOTFLAGS := $(shell root-config --cflags)
ROOTLIBS := $(shell root-config --evelibs)
FASTJETFLAGS := $(shell fastjet-config --cxxflags)
FASTJETLIBS := $(shell fastjet-config --libs)
ifneq ($(BOOST),)
BOOSTFLAGS := -I$(BOOST)/include/boost-1_55 -DBOOST_LOG_DYN_LINK
BOOSTSUFFIX := -gcc49-mt-1_55
BOOSTLIBS := -L$(BOOST)/lib -lboost_log$(BOOSTSUFFIX) -lboost_program_options$(BOOSTSUFFIX)
else
BOOSTFLAGS := -DBOOST_LOG_DYN_LINK
BOOSTLIBS := -lboost_log-mt -lboost_program_options-mt
endif

SRC := Topreco/Topreco.cxx \
	   Topreco/PrepareParticle.cxx \
	   Topreco/Methods/PseudoTop.cxx \
	   Topreco/Methods/Truth.cxx \
	   Topreco/MyPseudoJet.cxx \
	   Topreco/Methods/NeutrinoWeighting.cxx

HS := Topreco/Topreco.h \
	  Topreco/PrepareParticle.h \
	  Topreco/Methods/PseudoTop.h \
	  Topreco/Methods/Truth.h \
	  Topreco/MyPseudoJet.h \
	  Topreco/Methods/NeutrinoWeighting.h

OBJS := $(addprefix build/,$(patsubst %.cxx,%.o,$(SRC)))
BUILDDIRS := $(patsubst %,build/%,$(sort $(dir $(SRC))))

main: $(OBJS) $(HS) build/main.o
	$(CC) $(STD) $(FLAGS) -g -o $@ $(OBJS) build/main.o $(ROOTLIBS) $(FASTJETLIBS) $(BOOSTLIBS)

$(OBJS): build/%.o: %.cxx %.h | $(BUILDDIRS)
	$(CC) $(STD) $(ROOTFLAGS) $(FLAGS) $(FASTJETFLAGS) $(BOOSTFLAGS) -g -c -o $@ $<

build/main.o: main.cxx Method.hpp | $(BUILDDIRS)
	$(CC) $(STD) $(ROOTFLAGS) $(FLAGS) $(FASTJETFLAGS) $(BOOSTFLAGS) -g -c -o $@ $<

$(BUILDDIRS):
	mkdir -p $(BUILDDIRS)
	mkdir -p build/plots/notebooks

build/plots/all: plot.py build/storage.root | $(BUILDDIRS)
	./plot.py
	touch build/plots/all

build/storage.root: main
	./main

clean:
	rm -rf build
	rm main 
