#! /bin/bash
export ROOTLCG=/afs/cern.ch/sw/lcg/releases/LCG_79
export FASTJET=/afs/cern.ch/sw/lcg/external/fastjet/3.0.3/x86_64-slc6-gcc48-opt
export BOOST=$ROOTLCG/Boost/1.55.0_python2.7/x86_64-slc6-gcc49-opt

export PATH=${ROOTLCG}/gcc/4.9.3/x86_64-slc6/bin:${ROOTLCG}/ROOT/6.04.02/x86_64-slc6-gcc49-opt/bin:${FASTJET}/bin:$PATH
export LD_LIBRARY_PATH=${ROOTLCG}/ROOT/6.04.02/x86_64-slc6-gcc49-opt/lib:${ROOTLCG}/gcc/4.8.1/x86_64-slc6/lib64:${FASTJET}/lib:${BOOST}/lib:$LD_LIBRARY_PATH

source $ROOTLCG/Boost/1.55.0_python2.7/x86_64-slc6-gcc49-opt/Boost-env.sh
