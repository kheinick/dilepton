# Todo
1. Likelihood for 4b tuple
2. Reco Methode (pseudo-top Paper)
  1. eta-distributions of neutrinos

## NW-Method
1. Weniger Wahrheit Schritt für Schritt
    - Eta: true, sampled
      - Verschiedene Sortierungsmethoden testen?
        - |reco_1 - pdg| + |reco_2 - pdg|
        - Neutrinoweight
    - Top-Masse: smeared, true, PDG
    - W-Masse: PDG, true
    - b-truth
2. Sortierung der Neutrino-Kombinationen mit Wahrheit
  1. Unterschied Massen-Sortierung - Wahrheit, Wann ist andere Sortierung
     Falsch und was ist der Unterschied in der Kinematik?
  2. Andere Selektionsvariablen finden -> Für foam-Methode nutzbar?
    1. Delta R (lep, b nahe) und (lep, b weit)
3. Andere Datensätze NLO, Daten

## Interessante Histogramme
- 18
