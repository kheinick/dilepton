#include "Topreco.h"

Topreco::Topreco() :
  _method(new PseudoTop()),
  _reco(false)
{};

Topreco::Topreco(IReconstructionMethod* method) :
  _method(method),
  _reco(false)
{};

Topreco::~Topreco(){};

void Topreco::setJets(
  std::vector<Jet> jets,
  std::vector<Jet> chargedLeptons,
  std::vector<Jet> neutrinos
) {
  _jets = jets;
  _neutrinos = neutrinos;
  _chargedLeptons = chargedLeptons;
  std::stringstream ss;
  ss << "Received particles:";
  for(auto ar : {_jets, _chargedLeptons, _neutrinos}){
    for(auto p : ar){
      ss << " " << p.user_index();
    }
  }
  BOOST_LOG_TRIVIAL(debug) << ss.str();
}

bool Topreco::checkSizes(){
  return (_jets.size() >= 2
      && _neutrinos.size() >= 2
      && _chargedLeptons.size() >= 2);
}

std::vector<Jet> Topreco::getReconstructedTops(){
  _method->setJets(_jets, _chargedLeptons, _neutrinos);
  if (checkSizes() && _method->performChecks()){
    auto tops = _method->getResults();
    for(auto &t : tops){
      if(t.user_index() > 0){
        _topReco = t;
      } else {
        _topBarReco = t;
      }
    }
    _reco = true;
    return {_topReco, _topBarReco};
  } else {
    std::cout << "Should not get here" << std::endl;
    return {Jet(), Jet()};
  }
}

Jet Topreco::getReconstructedTop(){
  if(!_reco){
    getReconstructedTops();
  }
  return _topReco;
}

Jet Topreco::getReconstructedTopBar(){
  if(!_reco){
    getReconstructedTops();
  }
  return _topBarReco;
}

void Topreco::setMethod(IReconstructionMethod* method){
  _method = method;
}
