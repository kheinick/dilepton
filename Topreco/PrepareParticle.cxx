#include "PrepareParticle.h"

PrepareParticle::PrepareParticle() : 
  _lastVector(nullptr)
{};

PrepareParticle::PrepareParticle(const double px,
                                 const double py,
                                 const double pz,
                                 const double E,
                                 const int pdgId) :
  _lastVector(nullptr)
{
  addParticle(px, py, pz, E, pdgId);
};

PrepareParticle::~PrepareParticle(){};

void PrepareParticle::addParticle(const double px,
                                  const double py,
                                  const double pz,
                                  const double E,
                                  const int pdgId)
{
  // debug
  BOOST_LOG_TRIVIAL(debug) << "Receiving info for prepper: "
    << px << "(" << &px << ")" << " "
    << py << " "
    << pz << " "
    << E << " "
    << pdgId << " ";
  // enddebug

  // create instance of fasjet, setting the user_index to pdgId
  Jet jet(px, py, pz, E);
  jet.set_user_index(pdgId);

  // push jet into correct vector, corresponding to pdgId
  switch(std::abs(pdgId)){
    case 5:
      _jets.push_back(jet);
      _lastVector = &_jets;
      break;
    case 11:
    case 13:
    case 15:
      _chargedLeptons.push_back(jet);
      _lastVector = &_chargedLeptons;
      break;
    case 12:
    case 14:
    case 16:
      _neutrinos.push_back(jet);
      _lastVector = &_neutrinos;
      break;
    default:
      break;
  }
}

void PrepareParticle::addParticle(const double px,
                                  const double py,
                                  const double pz,
                                  const double E,
                                  const int pdgId,
                                  const int uniqueKey)
{
  addParticle(px, py, pz, E, pdgId);
  _lastVector->back().setUniqueKey(uniqueKey);
}

jetVector PrepareParticle::getChargedLeptons(){
  std::stringstream ss;
  ss << "This should be leptons: (Address " << &_chargedLeptons << "):";
  for(auto l : _chargedLeptons){
    ss << " " << l.user_index();
  }
  BOOST_LOG_TRIVIAL(debug) << ss.str();
  return _chargedLeptons;
}

jetVector PrepareParticle::getNeutrinos(){
  std::stringstream ss;
  ss << "This should be neutrinos (Address " << &_neutrinos << "):";
  for(auto l : _neutrinos){
    ss << " "  << l.user_index();
  }
  BOOST_LOG_TRIVIAL(debug) << ss.str();
  return _neutrinos;
}

jetVector PrepareParticle::getJets(){
  std::stringstream ss;
  ss << "This should be jets (Address " << &_jets << "):";
  for(auto l : _jets){
    ss << " "  << l.user_index();
  }
  BOOST_LOG_TRIVIAL(debug) << ss.str();
  return _jets;
}

void PrepareParticle::print(){

  std::cout << _neutrinos.size() << " | "
            << _chargedLeptons.size() << " | "
            << _jets.size() << std::endl;
}

void PrepareParticle::clear() {
  _jets.clear();
  _neutrinos.clear();
  _chargedLeptons.clear();
}

