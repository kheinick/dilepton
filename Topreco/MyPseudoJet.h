#ifndef MYPSEUDOJET_HPP_KWLRQ67Z
#define MYPSEUDOJET_HPP_KWLRQ67Z

#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <cstddef>
#include "fastjet/PseudoJet.hh"
#include <boost/log/trivial.hpp>

/** MyPseudoJet
 * This class extends the functionality of fastjet::PseudoJet
 * one can store childParticles directly in this class
 * and receive them later via pdg-id.
 */
class MyPseudoJet : public fastjet::PseudoJet {
  public:
    MyPseudoJet();
    MyPseudoJet(const fastjet::PseudoJet jet);
    MyPseudoJet(const double px,
                const double py,
                const double pz,
                const double E);
    ~MyPseudoJet();

    /* add a single MyPseudoJet to the vector of child particles */
    void addChild(MyPseudoJet &child);

    /* add a vector of MyPseudoJet's to the vector of child particles */
    void addChild(std::vector<MyPseudoJet> &children);

    /* receive all current children */
    const std::vector<MyPseudoJet>& getChildren() const;

    /** look for a child with given pdgId 
     * 
     * if absoluteId==true the sign of the pdgId doesn't matter
     */
    const MyPseudoJet getChild(int pdgId, bool absoluteId=false) const;

    /** look for the first child matching one of the given pdgIds 
     * 
     * if absoluteId==true the sign of the pdgId doesn't matter
     */
    const MyPseudoJet getChild(std::vector<int> pdgIds, bool absoluteId=false) const;

    /** dot-product
     */
    double dot(const MyPseudoJet&) const;

    /** angle
     */
    double ang(const MyPseudoJet&) const;

    /** print particle
     *
     * if withChildren==true it will recursively call that for the children
     */
    std::string print(bool withChildren=false, int verbosity=0) const;
    void setUniqueKey(int key);
    int uniqueKey() const;
    bool shareChildren(const MyPseudoJet& jet, bool recursively=false) const;
    double weight;

  private:
    static int _uniqueKeyCounter;
    int _uniqueKey;
    std::vector<MyPseudoJet> _children;

    template<class T>
    std::string col(std::vector<T> vals, int width=12) const {
      std::ostringstream s;
      for(auto val : vals){
        s << std::setw(width) << std::fixed << std::setprecision(2) << val;
      }
      return s.str();
    }

    template<class T>
    std::string col(T val, int width=12) const {
      return col(std::vector<T>{val}, width);
    }

    std::string _print(bool withChildren, int verbosity, int depth) const;
};

#endif /* end of include guard: MYPSEUDOJET_HPP_KWLRQ67Z */

