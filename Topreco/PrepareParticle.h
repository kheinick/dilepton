#ifndef PREPARE_PARTICLE__H
#define PREPARE_PARTICLE__H

#include <vector>
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "MyPseudoJet.h"

#include <boost/log/trivial.hpp>

typedef MyPseudoJet Jet;

typedef std::vector<Jet> jetVector;

class PrepareParticle {
  public:
    PrepareParticle();
    PrepareParticle(const double px,
                    const double py,
                    const double pz,
                    const double E,
                    const int pdgId);
    ~PrepareParticle();
    void addParticle(const double px,
                     const double py,
                     const double pz,
                     const double E,
                     const int pdgId);
    void addParticle(const double px,
                     const double py,
                     const double pz,
                     const double E,
                     const int pdgId,
                     const int uniqueKey);
    jetVector getChargedLeptons();
    jetVector getNeutrinos();
    jetVector getJets();
    void clear();
    void print();

  private:
    jetVector _neutrinos;
    jetVector _chargedLeptons;
    jetVector _jets;
    jetVector* _lastVector;
};

#endif // PREPARE_PARTICLE__H
