#include "MyPseudoJet.h"

int MyPseudoJet::_uniqueKeyCounter = 0;

MyPseudoJet::MyPseudoJet() :
  PseudoJet(),
  _uniqueKey(-_uniqueKeyCounter)
{
  _uniqueKeyCounter++;
};

MyPseudoJet::MyPseudoJet(const fastjet::PseudoJet jet) :
  PseudoJet(jet),
  _uniqueKey(-_uniqueKeyCounter)
{
  _uniqueKeyCounter++;
};

MyPseudoJet::MyPseudoJet(const double px,
            const double py,
            const double pz,
            const double E) :
  PseudoJet(px, py, pz, E),
  _uniqueKey(-_uniqueKeyCounter)
{
  _uniqueKeyCounter++;
};

MyPseudoJet::~MyPseudoJet(){};

void MyPseudoJet::addChild(MyPseudoJet &child){
  _children.push_back(child);
}

void MyPseudoJet::addChild(std::vector<MyPseudoJet> &children){
  for(auto &child : children){
    addChild(child);
  }
}

const std::vector<MyPseudoJet>& MyPseudoJet::getChildren() const {
  return _children;
}

const MyPseudoJet MyPseudoJet::getChild(int pdgId, bool absoluteId) const {
  for(auto c : _children){
    if(c.user_index() == pdgId){
      return c;
    }
    if(absoluteId && std::abs(c.user_index()) == std::abs(pdgId)){
      return c;
    }
  }
  return MyPseudoJet();
}

const MyPseudoJet MyPseudoJet::getChild(std::vector<int> pdgIds, bool absoluteId) const {
  for(auto id : pdgIds){
    MyPseudoJet c = getChild(id, absoluteId);
    if(c.E() != 0){
      return c;
    }
  }
  return MyPseudoJet();
}

bool MyPseudoJet::shareChildren(const MyPseudoJet& jet, bool recursively) const {
  BOOST_LOG_TRIVIAL(trace) << "checking" << this->print(true)
    << jet.print(true);
  if(*this == jet){
    BOOST_LOG_TRIVIAL(trace) << "We are one! "
      "Returning true";
    return true;
  }
  for(const auto &a : _children){
    if(a == jet){
      BOOST_LOG_TRIVIAL(trace) << "Daughter of given particle equals myself! "
        "Returning true";
      return true;
    }
    for(const auto &b : jet.getChildren()){
      if(recursively) {
        BOOST_LOG_TRIVIAL(trace) << "Checking whether my daughters share "
          "daughters with given particle...";
        if(a.shareChildren(b, true)) return true;
      }
    }
  }
  BOOST_LOG_TRIVIAL(trace) << "Did not find any shared daughters. Returning False.";
  return false;
}

std::string MyPseudoJet::print(bool withChildren, int verbosity) const {
  std::stringstream s;
  s << "\n" << col(std::vector<std::string>{"pdgId", "m", "pt", "px", "py", "eta", "phi", "weight", "uid"}) << std::endl;
  s << _print(withChildren, verbosity, 1);
  return s.str();
}

void MyPseudoJet::setUniqueKey(int key){
  _uniqueKey = key;
}

int MyPseudoJet::uniqueKey() const {
  return _uniqueKey;
}

std::string MyPseudoJet::_print(bool withChildren, int verbosity, int depth) const {
  std::stringstream s;
  s << col(std::vector<int>{user_index()}, 13-depth);
  s << col(std::vector<double>{m(), pt(), px(), py(), eta(), phi(), weight});
  if(_uniqueKey){
    s << col(_uniqueKey);
  }
  if(withChildren){
    s << std::endl;
    for(auto p : _children){
      for(int i=0; i<depth; i++){
        s << ">";
      }
      s << p._print(withChildren, verbosity, depth+1);
    }
  }
  return s.str();
}

double MyPseudoJet::dot(const MyPseudoJet& jet) const {
  return px() * jet.px() + py() * jet.py() + pz() + jet.pz();
}

double MyPseudoJet::ang(const MyPseudoJet& jet) const {
  return std::acos(this->dot(jet) / std::sqrt(this->dot(*this) * jet.dot(jet)));
}
