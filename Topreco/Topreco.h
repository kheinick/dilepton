#ifndef TOPRECO_H
#define TOPRECO_H

#include <iostream>
#include <array>
#include <vector>
#include "TLorentzVector.h"
#include "TParticle.h"
#include "Methods/PseudoTop.h"
#include "MyPseudoJet.h"

#include <boost/log/trivial.hpp>

typedef MyPseudoJet Jet;

class Topreco {
  public:
    Topreco();
    Topreco(IReconstructionMethod* method);
    ~Topreco();
    void setJets(
      std::vector<Jet> bJets,
      std::vector<Jet> chargedLeptons,
      std::vector<Jet> neutrinos
    );
    std::vector<Jet> getReconstructedTops();
    Jet getReconstructedTop();
    Jet getReconstructedTopBar();
    void setMethod(IReconstructionMethod* method);
  private:
    std::vector<Jet> _jets,
                     _neutrinos,
                     _chargedLeptons;
    IReconstructionMethod* _method;
    Jet _topReco,
        _topBarReco;
    bool _reco;
    bool checkSizes();
};

#endif
