#ifndef IRECONSTRUCTIONMETHOD_HPP
#define IRECONSTRUCTIONMETHOD_HPP

#include "../MyPseudoJet.h"
#include <iostream>
#include <typeinfo>

typedef MyPseudoJet Jet;

/**
 * Reconstruction-Method Interface
 * In order to create a new reconstruction Method, subclass
 * IReconstructionMethod and implement the pure virtual functions.
 *
 * The custom method should return a vector of top-pairs (of type MyPseudoJet)
 * via the getResults()-method.
 */
class IReconstructionMethod {
  public:
    IReconstructionMethod(){};

    /* Copy-constructor
     * mandatory for multithreading
     */
    IReconstructionMethod(IReconstructionMethod* method){};
    IReconstructionMethod(int verbosity) : 
      _verbosity(verbosity)
    {}
    virtual std::vector<Jet> getResults() = 0;
    virtual bool performChecks() = 0;
    virtual void setJets(const std::vector<Jet> &jets,
                         const std::vector<Jet> &chargedLeptons,
                         const std::vector<Jet> &neutrinos) = 0;
    virtual std::string description(){
      return "Descripton not available";
    };
    virtual IReconstructionMethod* copyPionter() = 0;


  protected:
    /* just some container vars to hold PDG-Masses
     */
    static constexpr double _mZ = 91.1876,
                            _mW = 80.385,
                            _mB = 4.18,
                            _mTop = 173.21;
    int _verbosity;

    /* simple signum function
     */
    template <typename T> static int sgn(T val) {
      return (T(0) < val) - (val < T(0));
    };

    /* Returns a list of all combinations of pairs of type T.
     * Each pair made of one element of v1 and one element of v2.
     * The combinations of pairs should be unique in the list
     * TODO: proof they are unique.
     */
    template <typename T>
    std::vector<std::pair<std::pair<T, T>, std::pair<T, T>>>
    uniquePairs(const std::vector<T> &v1, const std::vector<T> &v2)
    const noexcept
    {
      BOOST_LOG_TRIVIAL(trace) << "IReconstructionMethod::uniquePairs() for "
        << v1.size() << " times " << v2.size() << " combinations";
      std::vector<std::vector<std::pair<T, T>>> matrix;
      for(size_t i=0; i<v1.size(); i++){
        std::vector<std::pair<T, T>> vec;
        for(size_t j=0; j<v2.size(); j++){
          vec.push_back(std::make_pair(v1[i], v2[j]));
        }
        matrix.push_back(vec);
      }
      BOOST_LOG_TRIVIAL(trace) << "IReconstructionMethod::uniquePairs()"
        " Matrix created";
      std::vector<std::pair<std::pair<T, T>, std::pair<T, T>>> returnVec;
      for(size_t i=0; i<matrix.size(); i++){
        auto first = matrix[0][i];
        for(size_t j=1; j < matrix.size(); j++){
          for(size_t k=0; k < matrix.size(); k++){
            if(k==i) continue;
            auto second = matrix[j][k];
            returnVec.push_back(std::make_pair(first, second));
          }
        }
      }
      BOOST_LOG_TRIVIAL(trace) << "IReconstructionMethod::uniquePairs()"
        " found " << returnVec.size() << " unique Pairs.";
      return returnVec;
    }
};

#endif
