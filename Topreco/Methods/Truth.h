#ifndef TRUTH_H_IGGROER1
#define TRUTH_H_IGGROER1

#include <vector>
#include <algorithm>
#include "IReconstructionMethod.h"
#include "../MyPseudoJet.h"
#include <boost/log/trivial.hpp>

typedef MyPseudoJet Jet;

class Truth : public IReconstructionMethod {
  public:
    Truth ();
    Truth (Truth *);
    virtual ~Truth ();
    virtual std::vector<Jet> getResults();
    virtual bool performChecks();
    virtual void setJets(const std::vector<Jet> &jets,
                         const std::vector<Jet> &chargedLeptons,
                         const std::vector<Jet> &neutrinos);
    virtual std::string description();
    virtual Truth* copyPionter();
  private:
    std::vector<Jet> _jets,
                     _chargedLeptons,
                     _neutrinos;
    std::vector<Jet> _allParticles;
    Jet getTrueParticle(const int pdgId);
    Jet getTrueParticle(const std::vector<int> pdgIds);
};

#endif /* end of include guard: TRUTH_H_IGGROER1 */
