#include "NeutrinoWeighting.h"

NeutrinoWeighting::NeutrinoWeighting() noexcept {}

NeutrinoWeighting::NeutrinoWeighting(NeutrinoWeighting *nwmethod) noexcept :
  IReconstructionMethod((IReconstructionMethod *) nwmethod),
  useTruth(nwmethod->useTruth),
  newNwMethod(nwmethod->newNwMethod),
  storeEtaDistributions(nwmethod->storeEtaDistributions),
  scanEta(nwmethod->scanEta),
  _jets(nwmethod->_jets),
  _leptons(nwmethod->_leptons),
  _neutrinos(nwmethod->_neutrinos),
  _sample_size(nwmethod->_sample_size),
  _top_smearing_size(nwmethod->_top_smearing_size),
  _bestWeightCutoff(nwmethod->_bestWeightCutoff),
  _rnd(nwmethod->_rnd)
{}

NeutrinoWeighting::~NeutrinoWeighting() noexcept {}

std::vector<Jet> NeutrinoWeighting::getResults() noexcept {
  if(newNwMethod){
    _maximizeWeight();
    return {_bestTopPair.first, _bestTopPair.second};
  } else {
    return createTopCandidates();
  }
}

bool NeutrinoWeighting::performChecks() noexcept {
  return true;
}

std::string NeutrinoWeighting::description(){
  return "Neutrino-Weighting";
}

void NeutrinoWeighting::setJets(const std::vector<Jet> &jets,
                                const std::vector<Jet> &chargedLeptons,
                                const std::vector<Jet> &neutrinos) noexcept
{
  setJets(jets, chargedLeptons);
  double missingEtx = 0,
         missingEty = 0;
  for(auto n : neutrinos){
    missingEtx += n.px();
    missingEty += n.py();
  }
  _neutrinos = neutrinos;
  setMissingEt(missingEtx, missingEty);
  _usingTruth();
}

void NeutrinoWeighting::setJets(
    const std::vector<Jet> &jets,
    const std::vector<Jet> &chargedLeptons
) noexcept {
  _jets = jets;
  _leptons = chargedLeptons;
}

void NeutrinoWeighting::setMissingEt(
    const double &etx, const double &ety
) noexcept {
  _etx = etx;
  _ety = ety;
}

std::vector<Jet> NeutrinoWeighting::createTopCandidates() noexcept {
  BOOST_LOG_TRIVIAL(trace) << __FUNCTION__ << " requesting uniquePairs";

  std::vector<Jet> neutrinoCands;
  std::vector<std::pair<Jet, Jet>> topPairCands;

  topPairCands = _sampleTopPairCandidates();

  BOOST_LOG_TRIVIAL(trace) << "Finished combining";
  for(auto p : topPairCands){
    BOOST_LOG_TRIVIAL(trace) << p.first.print() << p.second.print();
  }

  // set default sortAlgorithm
  if(sortAlgorithm == nullptr){
    sortAlgorithm = &weightSort;
  }

  // calculate weights if needed
  if(sortAlgorithm == &weightSort){
    for(auto &p : topPairCands){
      Jet nu1 = p.first.getChild(24, true).getChild({12, 14, 16}, true);
      Jet nu2 = p.second.getChild(24, true).getChild({12, 14, 16}, true);
      double weight =
        std::exp(
          -std::pow(_etx - nu1.px() - nu2.px(), 2)
          / (2 * _sigmaX*_sigmaX)
        )
        * std::exp(
          -std::pow(_ety - nu1.py() - nu2.py(), 2)
          / (2 * _sigmaY*_sigmaY)
        );
      p.first.weight = weight;
      p.second.weight = weight;
    }
  }

  // sort tops with given function or by diff of topMass to pdgMass
  BOOST_LOG_TRIVIAL(trace) << "sorting";
  std::sort(topPairCands.begin(), topPairCands.end(), sortAlgorithm);

  if(topPairCands.size()){
    BOOST_LOG_TRIVIAL(trace) << "NeutrinoWeighting::Final Cands";
    BOOST_LOG_TRIVIAL(trace) << topPairCands.front().first.print();
    BOOST_LOG_TRIVIAL(trace) << topPairCands.front().second.print();
    return {topPairCands.front().first, topPairCands.front().second};
  } else {
    BOOST_LOG_TRIVIAL(warning) << __FUNCTION__ << " Found no combinations.";
    return {Jet(), Jet()};
  }
}

std::vector<std::pair<Jet, Jet>> NeutrinoWeighting::_sampleTopPairCandidates()
  noexcept
{
  std::vector<std::pair<Jet, Jet>> topPairCands;

  // for every b-jet, create a sample of neutrinos with every lepton
  // and build a top-candidate
  // if truth of b is used, just create the correct pair
  std::vector<std::pair<std::pair<Jet, Jet>, std::pair<Jet, Jet>>> blCombinations;
  if(useTruth["Bcharge"]){
    blCombinations = {
      std::make_pair(
        std::make_pair(_tops.front().getChild(5, true),
                       _tops.front().getChild(24, true)
                                    .getChild({11, 13, 15}, true)),
        std::make_pair(_tops.back().getChild(5, true),
                       _tops.back().getChild(24, true)
                                   .getChild({11, 13, 15}, true))
      )
    };
  } else {
    blCombinations = uniquePairs(_jets, _leptons);
  }
  for(auto &blPair : blCombinations){
    // putting everything in arrays is not elegant but may be
    // the simplest solution
    //
    // Generate all neutrinos for both blPairs seperate from each other
    std::array<std::pair<Jet, Jet>, 2> blPairArray = {{blPair.first, blPair.second}};
    std::array<std::vector<Jet>, 2> topPairArray;
    for(size_t i=0; i<2; i++){
      Jet b = blPairArray[i].first,
          l = blPairArray[i].second;
      BOOST_LOG_TRIVIAL(trace) << __FUNCTION__ <<
        " requesting neutrinoCandidates for " << b.print() << l.print();

      auto neutrinoCandidates = sampleNeutrinoCands(b, l);
      for(auto &n : neutrinoCandidates){
        Jet wCand = n + l;
        Jet topCand = wCand + b;
        if(topCand.m() > _topMassLowerLimit
            && topCand.m() < _topMassUpperLimit){
          wCand.addChild(n);
          wCand.addChild(l);
          wCand.set_user_index(-sgn(l.user_index()) * 24);
          topCand.addChild(wCand);
          topCand.addChild(b);
          topCand.set_user_index(sgn(b.user_index()) * 6);
          BOOST_LOG_TRIVIAL(trace) << __FUNCTION__ << ": pushing back"
            " topCandidates " << topCand.print();
          topPairArray[i].push_back(topCand);
        } else {
          BOOST_LOG_TRIVIAL(trace) << __FUNCTION__ << ": topMass "
            << topCand.m() << " not in between limits ["
            << _topMassLowerLimit << ", " << _topMassUpperLimit << "].";
        }
      }
    }

    // combine all tops from branch 1 with all the others from
    // the other branch
    for(auto &t1 : topPairArray[0]){
      for(auto &t2 : topPairArray[1]){
        topPairCands.push_back(std::make_pair(t1, t2));
      }
    }
  }
  return topPairCands;
}

std::vector<Jet> NeutrinoWeighting::getNeutrinoSolutions(
  const Jet &bJet,
  const Jet &lepton,
  const double &eta,
  const double mTop,
  const double mB,
  const double mW
) noexcept {
  auto plx = lepton.px(),
       ply = lepton.py(),
       pbx = bJet.px(),
       pby = bJet.py();

  auto Ebprime = bJet.E() * std::cosh(eta) - bJet.pz() * std::sinh(eta);
  auto Elprime = lepton.E() * std::cosh(eta) - lepton.pz() * std::sinh(eta);

  auto plpb = bJet.E() * lepton.E()
    - bJet.px() * lepton.px()
    - bJet.py() * lepton.py()
    - bJet.pz() * lepton.pz();

  auto a = (ply * Ebprime - pby * Elprime) / (pbx * Elprime - plx * Ebprime);
  auto b = (Elprime * (mTop*mTop - mW*mW - mB*mB - 2 * plpb)
            - Ebprime * mW*mW)
           / (2 * (plx * Ebprime - pbx * Elprime));

  auto c = a*a + 1 - std::pow(plx / Elprime * a + ply / Elprime, 2);
  auto d = 2 * a * b
    - 2 * (mW*mW / (2 * Elprime) + plx / Elprime * b)
        * (plx / Elprime * a + ply / Elprime);
  auto f = b*b - std::pow(mW*mW / (2 * Elprime) + plx / Elprime * b, 2);

  // discriminant to distinct number of solutions
  auto D = (d*d - 4*c*f);

  // save the actual solutions into a vector
  std::vector<double> pys(0);
  if(D > 0){
    auto py1 = - d/(2 * c) - 1/(2 * c) * std::sqrt(D);
    auto py2 = - d/(2 * c) + 1/(2 * c) * std::sqrt(D);
    pys.push_back(py1);
    pys.push_back(py2);
  } else if (D == 0){
    auto py =  - d/(2 * c);
    pys.push_back(py);
  } else {
    BOOST_LOG_TRIVIAL(trace) << __FUNCTION__ << " No suolution for D == "
      << D;
  }

  std::vector<Jet> neutrinos(0);
  BOOST_LOG_TRIVIAL(trace) << __FUNCTION__ << " Found " << pys.size() <<
    " Solutions:";
  for(auto py : pys){
    auto px = a * py + b;
    auto pt = std::sqrt(px * px + py * py);
    auto pz = pt * std::sinh(eta);
    auto E = std::sqrt(pt * pt + pz * pz);
    auto nu = Jet(px, py, pz, E);
    nu.set_user_index(
      -sgn(lepton.user_index()) * (std::abs(lepton.user_index()) + 1)
    );
    BOOST_LOG_TRIVIAL(trace) << __FUNCTION__ << " " << nu.print();
    neutrinos.push_back(nu);
  }

  return neutrinos;
}

std::vector<Jet> NeutrinoWeighting::sampleNeutrinoCands(
  const Jet &bJet, const Jet &lepton
) noexcept {
  // will hold all the candidates
  std::vector<Jet> cands(0);

  // Select top-mass sample to use.
  // If true top-mass should be used, the sample only contains 1 element
  double mt = 0;
  if(useTruth["TopMass"]){
    auto mass = lepton.user_index() > 0 ? _tops.back().m() : _tops.front().m();
    mt = mass;
  };

  // select eta sample
  // for true eta, the sample only contains 1 element, which is the eta of
  // the given neutrino
  double eta = 0;
  if(useTruth["Eta"]){
    auto currentTop = lepton.user_index() > 0 ? _tops.back() : _tops.front();
    auto n = currentTop.getChild(24, true).getChild({12, 14, 16}, true);
    eta = n.eta();
  };

  _updateSamples(eta, mt);

  double Wmass = _mW;
  if(useTruth["Wmass"]){
    auto currentTop = lepton.user_index() > 0 ? _tops.back() : _tops.front();
    auto W = currentTop.getChild(24, true);
    Wmass = W.m();
  }

  double bMass = _mB;
  if(useTruth["Bmass"]){
    bMass = bJet.m();
  }

  // iterate through all etas and top-masses and create all neutrino-solutions
  for(auto eta : _etas){
    for(auto topMass : _top_masses){
      for(auto neutrino : getNeutrinoSolutions(bJet,
                                               lepton,
                                               eta,
                                               topMass,
                                               bMass,
                                               Wmass))
      {
        cands.push_back(neutrino);
      }
    }
  }
  return cands;
}

bool NeutrinoWeighting::_usingTruth() noexcept {
  bool v = false;
  for(auto t : useTruth){
    if(t.second){
      v = true;
    }
  }

  // Make sure we have a truthy dataset when using truth
  if(v){
    assert(_neutrinos.size() == 2);
    assert(_leptons.size() == 2);
    assert(_jets.size() == 2);

    Jet b = _jets.front().user_index() == 5 ? _jets.front() : _jets.back();
    Jet bBar = _jets.front().user_index() == -5 ? _jets.front() : _jets.back();
    Jet lep = _leptons.front().user_index() > 1
      ? _leptons.front() : _leptons.back();
    Jet antiLep = _leptons.front().user_index() < 1
      ? _leptons.front() : _leptons.back();
    Jet nu = _neutrinos.front().user_index() > 1
      ? _neutrinos.front() : _neutrinos.back();
    Jet nuBar = _neutrinos.front().user_index() < 1
      ? _neutrinos.front() : _neutrinos.back();
    Jet wPlus = nu + antiLep;
    wPlus.set_user_index(24);
    wPlus.addChild(nu);
    wPlus.addChild(antiLep);
    Jet wMinus = nuBar + lep;
    wMinus.set_user_index(-24);
    wMinus.addChild(nuBar);
    wMinus.addChild(lep);
    Jet t = wPlus + b;
    t.set_user_index(6);
    t.addChild(wPlus);
    t.addChild(b);
    Jet tBar = wMinus + bBar;
    tBar.set_user_index(-6);
    tBar.addChild(wMinus);
    tBar.addChild(bBar);

    _tops.clear();
    _tops.push_back(t);
    _tops.push_back(tBar);

    BOOST_LOG_TRIVIAL(debug) << __FUNCTION__
      << t.print(true) << tBar.print(true);
  }
  return v;
}

bool NeutrinoWeighting::topMassDifferenceSort(
  std::pair<Jet, Jet> &i,
  std::pair<Jet, Jet> &j
) noexcept {
  return (std::abs(i.first.m() - _mTop) + std::abs(i.second.m() - _mTop))
          < (std::abs(j.first.m() - _mTop) + std::abs(j.second.m() - _mTop));
};

bool NeutrinoWeighting::weightSort(
  std::pair<Jet, Jet> &i,
  std::pair<Jet, Jet> &j
) noexcept {
  return i.first.weight > j.first.weight;
};

void NeutrinoWeighting::_shuffleSamples() noexcept {
  if(!useTruth["Eta"]){
    size_t i=0;
    for(auto &v : _etas){
      if(scanEta){
        v = -_eta_scan_limit + 2 * _eta_scan_limit / _sample_size * i;
        i++;
      } else {
        v = _rnd->Gaus(_eta_mean, _eta_sigma);
      }
    }
  }
  if(!useTruth["TopMass"]){
    for(auto &v : _top_masses){
      v = _rnd->BreitWigner(_top_mean, _top_gamma);
    }
  }
}

NeutrinoWeighting* NeutrinoWeighting::copyPionter() noexcept {
  return new NeutrinoWeighting(this);
}

void NeutrinoWeighting::setTopSampleSize(size_t s) noexcept {
  _top_smearing_size = s;
}

void NeutrinoWeighting::setEtaSampleSize(size_t s) noexcept {
  _sample_size = s;
}

void NeutrinoWeighting::_maximizeWeight() noexcept {
  _updateSamples();
  _maximumWeight = 0;
  _bestTopPair = std::make_pair(Jet(), Jet());

  TH2D *etaDistHist = nullptr;

  for(auto &blPair : uniquePairs(_jets, _leptons)){
    // store leptons and bs from bl-pair
    auto b1 = blPair.first.first,
         b2 = blPair.second.first,
         l1 = blPair.first.second,
         l2 = blPair.second.second;

    std::stringstream name;
    name << "EtaDist-" << _leptons.front().uniqueKey() / 10
      << "-l" << sgn(l1.user_index());
    if(storeEtaDistributions){
      size_t nBins = _sample_size / 10;
      etaDistHist = new TH2D(name.str().c_str(), name.str().c_str(), nBins, -5, 5, nBins, -5, 5);
    }

    auto topMassesA = _top_masses;
    if(useTruth["TopMass"]){
      auto mass = l1.user_index() > 0 ? _tops.back().m() : _tops.front().m();
      topMassesA = {mass};
    }
    auto topMassesB = _top_masses;
    if(useTruth["TopMass"]){
      auto mass = l2.user_index() > 0 ? _tops.back().m() : _tops.front().m();
      topMassesB = {mass};
    }

    // iterate through top-masses and neutrino-etas for first top-cand
    for(auto &topMassA : topMassesA){
      for(auto &etaA : _etas){
        auto Wmass = _mW;
        if(useTruth["Wmass"]){
          auto currentTop = l1.user_index() > 0 ? _tops.back() : _tops.front();
          auto W = currentTop.getChild(24, true);
          Wmass = W.m();
        }
        auto neutrinoCandidatesA = getNeutrinoSolutions(b1, l1, etaA, topMassA,
          _mB, Wmass);
        if(neutrinoCandidatesA.size()){
          for(auto &neutrinoA : neutrinoCandidatesA){
            // found first neutrino candidate at this point. repeat for second
            // top-candidate
            for(auto &topMassB : topMassesB){
              for(auto &etaB : _etas){
                auto Wmass = _mW;
                if(useTruth["Wmass"]){
                  auto currentTop = l2.user_index() > 0 ? _tops.back() : _tops.front();
                  auto W = currentTop.getChild(24, true);
                  Wmass = W.m();
                }
                auto neutrinoCandidatesB = getNeutrinoSolutions(b2, l2, etaB,
                  topMassB, _mB, Wmass);
                if(neutrinoCandidatesB.size()){
                  for(auto neutrinoB : neutrinoCandidatesB){
                    Jet WA = neutrinoA + l1,
                        topA = WA + b1,
                        WB = neutrinoB + l2,
                        topB = WB + b2;
                    auto weight = _weight(_etx, _ety,
                                          neutrinoA.px() + neutrinoB.px(),
                                          neutrinoA.py() + neutrinoB.py());
                    if(storeEtaDistributions){
                      etaDistHist->Fill(etaA, etaB, weight);
                    }
                    if(weight > _maximumWeight){
                      BOOST_LOG_TRIVIAL(debug) << __FUNCTION__ << " " << weight;
                      WA.addChild(neutrinoA);
                      WA.addChild(l1);
                      WA.set_user_index(-sgn(l1.user_index()) * 24);
                      WB.addChild(neutrinoB);
                      WB.addChild(l2);
                      WB.set_user_index(-sgn(l2.user_index()) * 24);
                      topA.addChild(WA);
                      topA.addChild(b1);
                      topA.set_user_index(-sgn(l1.user_index()) * 6);
                      topB.addChild(WB);
                      topB.addChild(b2);
                      topB.set_user_index(-sgn(l2.user_index()) * 6);
                      topA.weight = weight;
                      topB.weight = weight;
                      _bestTopPair = std::make_pair(topA, topB);
                      _maximumWeight = weight;
                      if(weight > _bestWeightCutoff){
                        return;
                      }
                    }
                  }
                } else {
                  if(storeEtaDistributions){
                    etaDistHist->Fill(etaA, -10000, 0);
                  }
                }
              }
            }
          }
        } else {
          if(storeEtaDistributions){
            etaDistHist->Fill(10000, 10000, 0);
          }
        }
      }
    }
    if(storeEtaDistributions){
      etaDistHist->Write(name.str().c_str(), TObject::kOverwrite);
      delete etaDistHist;
    }
  }
}

void NeutrinoWeighting::_updateSamples(double eta, double topMass){
  if(useTruth["TopMass"]){
    _top_masses = {topMass};
  } else {
    _top_masses = std::vector<double>(_top_smearing_size);
  }

  if(useTruth["Eta"]){
    _etas = {eta};
  } else {
    _etas = std::vector<double>(_sample_size);
  }

  _shuffleSamples();
}

void NeutrinoWeighting::setBestWeightCutoff(double val) noexcept {
  _bestWeightCutoff = val;
}

double NeutrinoWeighting::_weight(const double exMiss,
                                  const double eyMiss,
                                  const double exReco,
                                  const double eyReco,
                                  const double sigmaExMiss,
                                  const double sigmaEyMiss) noexcept
{
  return std::exp(
    -std::pow(exMiss - exReco, 2)
    / (2 * sigmaExMiss*sigmaExMiss)
  )
  * std::exp(
    -std::pow(eyMiss - eyReco, 2)
    / (2 * sigmaEyMiss*sigmaEyMiss)
  );
}
