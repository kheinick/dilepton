#include "Truth.h"

Truth::Truth(){};

Truth::Truth(Truth *t) : 
  IReconstructionMethod(t)
{};

Truth* Truth::copyPionter(){
  return new Truth(this);
}

Truth::~Truth(){};

std::vector<Jet> Truth::getResults(){
  std::vector<Jet> tops;
  int sign = 1;
  // some random loop. essentially only loops over the number of
  // possibly reconstructed jets
  // TODO: improve logic, move this to some smart functions
  for(size_t i=0; i<_jets.size(); i++){
    sign *= -1;
    std::vector<int> lIds = {sign * 11, sign * 13, sign * 15};

    // get any charged lepton matching the given sign
    // and the corresponding neutrino
    Jet l(getTrueParticle(lIds));
    int nuPdgId = -sign * (std::abs(l.user_index()) + 1);
    Jet nu(getTrueParticle(nuPdgId));
    Jet w = l + nu;
    w.addChild(l); w.addChild(nu);
    w.set_user_index(-sign * 24);
    Jet b = getTrueParticle(-sign * 5);
    Jet top = w + b;
    top.addChild(w); top.addChild(b);
    top.set_user_index(sgn(b.user_index()) * 6);
    tops.push_back(top);
  }
  return tops;
}

bool Truth::performChecks(){
  for(auto &p : _allParticles){
    if(p.user_index()==0){
      return false;
    }
  }
  return true;
}

void Truth::setJets(const std::vector<Jet> &jets,
                    const std::vector<Jet> &chargedLeptons,
                    const std::vector<Jet> &neutrinos){
  assert(jets.size() >= 2
         && chargedLeptons.size() >= 2
         && neutrinos.size() >= 2);
  _jets = jets;
  _chargedLeptons = chargedLeptons;
  _neutrinos = neutrinos;
  _allParticles.clear();
  for(std::vector<Jet> arr
      : {_jets, _chargedLeptons, _neutrinos}){
    for(Jet &p : arr){
      _allParticles.push_back(p);
      BOOST_LOG_TRIVIAL(trace) << "pushed " << p.user_index()
        << " into _allParticles.";
    }
  }
  std::stringstream ss;
  for(Jet &p : _allParticles){
    ss << " " << p.user_index();
  };
  BOOST_LOG_TRIVIAL(trace) << "_allParticles contain: " << ss.str();
}

Jet Truth::getTrueParticle(const int pdgId){
  for(Jet &p : _allParticles){
    if(p.user_index() == pdgId) return p; 
  }
  BOOST_LOG_TRIVIAL(warning) << "Could not find any particle,"
    " matching the given pdgId: " << pdgId << ")";
  std::stringstream ss;
  ss << "Looking in";
  for(Jet &p : _allParticles){
    ss << " " << p.user_index();
  };
  BOOST_LOG_TRIVIAL(trace) << ss.str();
  return Jet();
}

Jet Truth::getTrueParticle(const std::vector<int> pdgIds){
  for(int id : pdgIds){
    for(Jet &p : _allParticles){
      if(p.user_index() == id){
        return p;
      }
    }
  }
  std::cerr << "Could not find any particle,"
    " matching the given pdgIds: ( ";
  for(int id : pdgIds){
    std::cout << id << " ";
  }
  std::cout << ")" << std::endl << std::flush;
  return Jet();
}

std::string Truth::description(){
  return "Truth-Method";
}
