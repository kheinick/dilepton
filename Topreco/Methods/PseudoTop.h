#ifndef PSEUDOTOP_HPP
#define PSEUDOTOP_HPP

#include <vector>
#include <array>
#include <algorithm>
#include <functional>
#include "IReconstructionMethod.h"
#include "../MyPseudoJet.h"
#include <boost/log/trivial.hpp>

typedef MyPseudoJet Jet;

class PseudoTop : public IReconstructionMethod {
  public:
    PseudoTop();
    PseudoTop(PseudoTop *);
    PseudoTop(int verbosity);
    virtual ~PseudoTop();
    virtual std::vector<Jet> getResults();
    virtual bool performChecks();
    virtual void setJets(const std::vector<Jet> &jets,
                         const std::vector<Jet> &chargedLeptons,
                         const std::vector<Jet> &neutrinos);
    virtual std::string description();
    PseudoTop* copyPionter();
  private:
    std::vector<Jet> _jets,
                     _chargedLeptons,
                     _neutrinos;
    std::vector<Jet> minimizePdgDiff(
        std::vector<Jet> &partsA,
        std::vector<Jet> &partsB,
        float pdgMass,
        std::vector<Jet> (*combinator)(std::vector<Jet>&, std::vector<Jet>&)
    );
    bool oppositeChargedLeptons();
    bool sameFlavourLeptons();
    float leptonMass();
    bool largerPt(Jet i, Jet j);
    static std::vector<Jet> wCandidates(
      std::vector<Jet> &neutrinos,
      std::vector<Jet> &leptons
    );
    static std::vector<Jet> topCandidates(
      std::vector<Jet> &ws,
      std::vector<Jet> &bs
    );
};


#endif
