#include "PseudoTop.h"

PseudoTop::PseudoTop(){};

PseudoTop::PseudoTop(int verbosity) : 
  IReconstructionMethod(verbosity)
{};

PseudoTop* PseudoTop::copyPionter(){
  return new PseudoTop(this);
}

PseudoTop::PseudoTop(PseudoTop* pt) : 
  IReconstructionMethod(pt)
{};

PseudoTop::~PseudoTop(){};

std::vector<Jet> PseudoTop::getResults(){
  // generate all possible W-Candidates and sort by Pt.
  // chose both candidates with max Pt and differenct charge.
  std::vector<Jet> ws = minimizePdgDiff(
    _neutrinos, _chargedLeptons, _mW, wCandidates
  );
  // TODO: force alternating charges, different daughter particles

  // return values
  // generate all possible top-Candidates and sort by Pt.
  // chose both candidates with max Pt and differenct charge.
  std::vector<Jet> tops = minimizePdgDiff(
    ws, _jets, _mTop, topCandidates
  );
  // TODO: force alternating charges
  return {tops[0], tops[1]};
};

bool PseudoTop::performChecks(){
  // minimum 2 jets
  if (_jets.size() < 2){
    BOOST_LOG_TRIVIAL(warning)
      << "Not enough Jets. Aborting.";
    return false;
  }

  // minimum 2 charged leptons
  if (_chargedLeptons.size() < 2){
    BOOST_LOG_TRIVIAL(warning)
      << "Not enough Leptons. Aborting.";
    return false;
  }

  // minimum 2 neutrinos
  if (_neutrinos.size() != 2){
    BOOST_LOG_TRIVIAL(warning)
      << "Number of Neutrinos must be 2. Aborting.";
    return false;
  }

  // leptons must be with opposing charge
  if (!oppositeChargedLeptons()){
    BOOST_LOG_TRIVIAL(warning)
      << "Leptons have same charge. Aborting.";
    return false;
  }

  // check lepton-mass if they have same flavour
  // the mass must not fit the Z-mass
  /* if(sameFlavourLeptons()){
    if(std::abs(leptonMass() - _mZ) < 10){
      BOOST_LOG_TRIVIAL(error)
        << "leptonMass too different from Zmass. Aborting.";
      return false;
    }
    if(leptonMass() < 20){
      BOOST_LOG_TRIVIAL(error)
        << "leptonMass too small. Aborting.";
      return false;
    }
  } */
  return true;
};

void PseudoTop::setJets(const std::vector<Jet> &jets,
                        const std::vector<Jet> &chargedLeptons,
                        const std::vector<Jet> &neutrinos){
  _jets = jets;
  _chargedLeptons = chargedLeptons;
  _neutrinos = neutrinos;

  BOOST_LOG_TRIVIAL(trace)
    << "PseudoTop::setJets() receiving jets. "
    << jets.size() << " jets, "
    << chargedLeptons.size() << " chargedLeptons, "
    << neutrinos.size() << " neutrinos, ";

  // sort by pt
  auto ll = [](Jet i, Jet j){
    return i.pt() > j.pt();
  };
  std::sort(_jets.begin(), _jets.end(), ll);
  std::sort(_chargedLeptons.begin(), _chargedLeptons.end(), ll);
  std::sort(_neutrinos.begin(), _neutrinos.end(), ll);
};

bool PseudoTop::oppositeChargedLeptons() {
  int positive = 0;
  for(auto l : _chargedLeptons){
    if(l.user_index() > 0) positive++;
  }
  return positive == _chargedLeptons.size() / 2;
}

bool PseudoTop::sameFlavourLeptons() {
  return (std::abs(_chargedLeptons[0].user_index())
    == std::abs(_chargedLeptons[1].user_index()));
}

float PseudoTop::leptonMass(){
  return (_chargedLeptons[0] + _chargedLeptons[1]).m();
}

bool PseudoTop::largerPt(Jet i, Jet j){
  return i.pt() > j.pt();
}

std::vector<Jet> PseudoTop::wCandidates(
    std::vector<Jet> &neutrinos,
    std::vector<Jet> &leptons
) {
  std::vector<Jet> candidates;
  for(auto &mu : neutrinos){
    for(auto &l : leptons){
      Jet cand(mu+l);
      cand.addChild(mu);
      cand.addChild(l);
      int sign = l.user_index() > 0 ? -1 : 1;
      BOOST_LOG_TRIVIAL(trace)
        << "lepton user_index == " << l.user_index()
        << " -> creating " << sign * 24 << "-particle";
      cand.set_user_index(sign * 24);
      candidates.push_back(cand);
    }
  }
  return candidates;
}

std::vector<Jet> PseudoTop::topCandidates(
    std::vector<Jet> &ws,
    std::vector<Jet> &bs
) {
  std::vector<Jet> candidates;

  // iterate all Ws and Bs
  for(auto &w : ws){
    for(auto &b : bs){
      // combine w+b and set correct pdgId
      Jet cand(w+b);
      cand.addChild(w);
      cand.addChild(b);
      int sign = w.user_index() > 0 ? 1 : -1;
      cand.set_user_index(sign * 6);
      candidates.push_back(cand);

      // debug
      BOOST_LOG_TRIVIAL(trace)
        << "top user_index == " << w.user_index()
        << " -> creating " << sign * 6 << "-particle";
      // enddebug
    }
  }
  return candidates;
}

std::vector<Jet> PseudoTop::minimizePdgDiff(
    std::vector<Jet> &partsA,
    std::vector<Jet> &partsB,
    float pdgMass,
    std::vector<Jet> (*combinator)(std::vector<Jet>&, std::vector<Jet>&)
) {
  // get a vector of all possible candidates
  std::vector<Jet> candidates = combinator(partsA, partsB);

  // debug
  BOOST_LOG_TRIVIAL(trace)
    << "Created " << candidates.size() << " candidates." <<  std::endl;
  // enddebug

  // create a vector of combinations of particles
  std::vector<std::array<Jet, 2>> combinations;

  // only combine the particles when they dont share any daughters
  // TODO : Only combine once!
  for(Jet &a : candidates){
    for(Jet &b : candidates){
      if(!a.shareChildren(b, true)){
        BOOST_LOG_TRIVIAL(trace) << a.print(true) << b.print(true) << " dont share children";
        combinations.push_back({{a, b}});
      }
    }
  }

  // debug
  BOOST_LOG_TRIVIAL(trace)
    << "PseudoJet::minimizePdgDiff() found "  << combinations.size()
    << " combinations. Sorting.";
  // enddebug

  // sort particles corresponding to |m_reco1 - m_pdg| + |m_reco2 - m_pdg|
  std::sort(combinations.begin(), combinations.end(),
      [&](std::array<Jet, 2> i,
          std::array<Jet, 2> j){
    double diffI = std::abs(i[0].m() - pdgMass)
                   + std::abs(i[1].m() - pdgMass);
    double diffJ = std::abs(j[0].m() - pdgMass)
                   + std::abs(j[1].m() - pdgMass);
    return diffI < diffJ;    
  });
  return {combinations.front()[0], combinations.front()[1]};
}

std::string PseudoTop::description(){
  return "PseudoTop-Method";
}
