#ifndef NEUTRINOWEIGHTING_H_7UNXWOBT
#define NEUTRINOWEIGHTING_H_7UNXWOBT

#include <vector>
#include <array>
#include <complex>
#include <algorithm>
#include <map>

#include "TRandom3.h"
#include "TNtuple.h"
#include "TH2D.h"
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"

#include "IReconstructionMethod.h"
#include "../MyPseudoJet.h"

typedef MyPseudoJet Jet;

class NeutrinoWeighting : public IReconstructionMethod {
  public:
    /* Interface implementations, check IReconstructionMethod for docs.
     */
    NeutrinoWeighting () noexcept;
    NeutrinoWeighting (NeutrinoWeighting *) noexcept;
    virtual ~NeutrinoWeighting () noexcept;
    NeutrinoWeighting* copyPionter() noexcept;
    virtual std::vector<Jet> getResults() noexcept;
    virtual bool performChecks() noexcept;
    virtual void setJets(const std::vector<Jet> &jets,
                         const std::vector<Jet> &chargedLeptons,
                         const std::vector<Jet> &neutrinos) noexcept;
    virtual std::string description();

    /* Additional setJets function. It should be ok to only set
     * the b-jets and chargedLeptons, seperately from the missing Ets
     */
    void setJets(const std::vector<Jet> &jets,
                 const std::vector<Jet> &chargedLeptons) noexcept;

    /* Setter fron missing Et seperated from jets
     */
    void setMissingEt(const double &etx, const double &ety) noexcept;

    /* Level of truth switches */
    std::map<std::string, bool> useTruth {
      {"TopMass", false},
      {"Bcharge", false},
      {"Bmass", false},
      {"Wmass", false},
      {"Eta", false}
    };

    bool (*sortAlgorithm)
      (std::pair<Jet, Jet>&, std::pair<Jet, Jet>&) = nullptr;
    static bool topMassDifferenceSort(
      std::pair<Jet, Jet> &i, std::pair<Jet, Jet> &j
    ) noexcept;
    static bool weightSort(
      std::pair<Jet, Jet> &i, std::pair<Jet, Jet> &j
    ) noexcept;

    void setTopSampleSize(size_t) noexcept;
    void setEtaSampleSize(size_t) noexcept;

    bool newNwMethod = true,
         storeEtaDistributions = false,
         scanEta;

    void setBestWeightCutoff(double) noexcept;

  private:
    std::vector<Jet> _jets,
                     _leptons,
                     _neutrinos,
                     _tops;
    size_t _sample_size,
           _top_smearing_size;
    constexpr static double _eta_mean = 0,
                            _eta_scan_limit = 5,
                            _eta_sigma = 1.355,
                            _top_mean = 172.5,
                            _top_gamma = 1.547,
                            _topMassUpperLimit = 10000,
                            _topMassLowerLimit = 0,
                            _sigmaX = 1,
                            _sigmaY = 1;

    double _etx,
           _ety,
           _maximumWeight = 0,
           _bestWeightCutoff = 0.999;
    bool _usingTruth() noexcept;
    std::string _neutrinoMethod;
    std::vector<Jet> createTopCandidates() noexcept;
    std::vector<double> _etas;
    std::vector<double> _top_masses;
    TRandom3 *_rnd = new TRandom3();
    std::vector<Jet> sampleNeutrinoCands(
      const Jet &bJet, const Jet &lepton
    ) noexcept;

    /**
     * Implementation of neutrino weighting method used in
     * http://hss.ulb.uni-bonn.de/2007/1277/1277.pdf
     */
    std::vector<Jet> getNeutrinoSolutions(
      const Jet &bJet,
      const Jet &lepton,
      const double &eta,
      const double mTop=_mTop,
      const double mB=_mB,
      const double mW=_mW
    ) noexcept;

    std::vector<std::pair<Jet, Jet>> _sampleTopPairCandidates() noexcept;

    void _shuffleSamples() noexcept;
    void _updateSamples(double eta=10000, double topMass=0);

    /* New, main loop for iterating eta-top combinations and creating
     * top-candidates.
     *
     * Will calculate weight for each combination and only keep the
     * highest weight. Breaks as soon as best weight has reached
     * _bestWeightCutoff
     */
    void _maximizeWeight() noexcept;
    std::pair<Jet, Jet> _bestTopPair;
    double _weight(const double exMiss,
                   const double eyMiss,
                   const double exReco,
                   const double eyReco,
                   const double sigmaExMiss = _sigmaX,
                   const double sigmaEyMiss = _sigmaY) noexcept;
};

#endif /* end of include guard: NEUTRINOWEIGHTING_H_7UNXWOBT */
