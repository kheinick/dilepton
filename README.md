# Tests of Neutrinoweighting

## Testing the Neutrinoweighting-Method for reconstruction of dilepton-events

This project consists of a small framework for testing different
reconstruction-methods in *tt̅ → bb̅ ℓ⁺ℓ⁻ νν̅* events.
The framework is based on the `C++`-frameworks [`root`](https://root.cern.ch)
(Version 6.04) and [`fastjet`](http://fastjet.fr) (Version 3.1.2).

### Project Status
Currently there are three Methods implemented:
- *Neutrinoweighting*, which samples Neutrino-eta values to estimate the
  fourvectors of the two, not detectable, neutrinos
- *Pseudo-Top*, which reconstructs the top-quarks by applying simple
  assumptions
- *Truth*, which uses the monte-carle truth of the given dataset

The program `main` uses the framework, takes care of the setup and manages
some user input to control the different methods (see `./main --help`).

### Building
The project uses `C++14`-Syntax and therefore depends on
- `root` (> v6)
- `fastjet` (> v3)
- `bootstrap` (> v1.5)

If all the dependencies are satisfied, one can build simply with `make`.
Check the [Wiki](https://gitlab.cern.ch/kheinick/dilepton/wikis/build) for more
details.

### Analyses
There are some analyses implemented. They depend on several datasets which have
to be created before and are documented in the corrisponding files.
The analysis is Python-based and depends on
- `python2`
- `numpy`
- `root_numpy`
- `uncertainties`
- `matplotlib`
- `ipython`

Simply run `ipython notebook` inside the project-folder to see the plots and
some comments on them.